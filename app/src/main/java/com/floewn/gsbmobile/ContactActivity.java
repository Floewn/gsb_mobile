package com.floewn.gsbmobile;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class ContactActivity extends AppCompatActivity
{
    EditText nom;
    EditText prenom;
    EditText courriel;
    EditText tel;
    SQlite db;
    ImageButton btnReturn;
    Button btnModify;
    Button btnCall;
    Button btnDelete;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        this.nom = findViewById(R.id.txtNom);
        this.prenom = findViewById(R.id.txtPrenom);
        this.tel = findViewById(R.id.txtNumero);
        this.courriel = findViewById(R.id.txtCourriel);
        btnReturn = findViewById(R.id.btnReturnContact);
        btnModify = findViewById(R.id.btnModify);
        btnDelete = findViewById(R.id.btnDelete);
        btnCall = findViewById(R.id.btnCall);

        this.db = new SQlite(this);

        this.id = Context.getInstance().getLastContactId();

        Cursor data = db.getContactById(id);

        while(data.moveToNext())
        {
            nom.setText(data.getString(2));
            prenom.setText(data.getString(3));
            tel.setText(data.getString(4));
            courriel.setText(data.getString(5));
        }

        btnReturn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent addContactActivity = new Intent(getApplicationContext(), RepertoireActivity.class);
                startActivity(addContactActivity);
                finish();
            }
        });

        btnModify.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                db.updateContact(id, nom.getText().toString(), prenom.getText().toString(), tel.getText().toString(), courriel.getText().toString());
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                db.deleteContact(id, nom.getText().toString(), prenom.getText().toString(), tel.getText().toString(), courriel.getText().toString());

                nom.setText("");
                prenom.setText("");
                courriel.setText("");
                tel.setText("");
            }

        });

        btnCall.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View v)
            {
                try
                {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+tel.getText().toString()));
                    startActivity(callIntent);
                } catch (ActivityNotFoundException activityException)
                {
                    Log.e("Appel du numero", "Erreur appel", activityException);
                }
            }
        });
    }
}
