package com.floewn.gsbmobile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class SQlite extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "Contact.db";
    private static final String TABLE_NAME = "CONTACT";

    SQlite (Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        Log.println(1, "lol", "wow");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, CREATORID INTEGER, NOM TEXT, PRENOM TEXT, TEL TEXT, MAIL TEXT) ");
        db.execSQL("CREATE TABLE IF NOT EXISTS USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NOM TEXT, PASS TEXT) ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS USERS");
        onCreate(db);
    }

    Cursor getAllContactById(int creatorID)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select * from CONTACT as c where c.CREATORID = "+ creatorID, null);
    }

    Cursor getContactById(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select * from CONTACT as c where c.ID = "+ id, null);
    }

    Cursor getAllUser()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select * from USERS", null);
    }

    boolean createOrUpdateUser(String name, String pass)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from USERS as u where u.NOM = '" + name + "'", null);
        if(!data.moveToNext())
        {
            ContentValues contentValues = new ContentValues();
            contentValues.put("NOM", name);
            contentValues.put("PASS", pass);
            long result = db.insert("USERS", null, contentValues);
            if(result == -1)
            {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    boolean createContact(int id, String nom, String prenom, String tel, String mail)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("CREATORID", id);
        contentValues.put("NOM", nom);
        contentValues.put("PRENOM", prenom);
        contentValues.put("TEL", tel);
        contentValues.put("MAIL", mail);
        long result = db.insert("CONTACT", null, contentValues);
        if(result == -1)
        {
            return false;
        }else {
            return true;
        }
    }

    void updateContact(int id, String nom, String prenom, String tel, String mail)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String updateContact = "UPDATE "+TABLE_NAME+ " SET NOM = '"+nom+"' , PRENOM = '"+prenom+"' , TEL = '"+tel+"' , MAIL = '"+mail+"' WHERE ID = "+ id;
        db.execSQL(updateContact);
    }

    void deleteContact(int id, String nom, String prenom, String tel, String mail)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteContact = "DELETE FROM '"+TABLE_NAME+ "' WHERE ID = "+ id;
        db.execSQL(deleteContact);
    }

    public void clearDatabase(String TABLE_NAME)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String clearDBQuery = "DROP TABLE "+TABLE_NAME;
        db.execSQL(clearDBQuery);
    }

    public SQLiteDatabase getDb()
    {
        return getWritableDatabase();
    }
}
