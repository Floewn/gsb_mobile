package com.floewn.gsbmobile;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    private Button btnConnect;
    private TextView btnInscription;
    private SQlite db;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.db = new SQlite(this);
        this.db.onCreate(this.db.getDb());
        boolean userCreate = db.createOrUpdateUser("admin", "admin");
        this.btnInscription = findViewById(R.id.textView2);
        this.btnConnect = findViewById(R.id.btnModify);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText ETName = findViewById(R.id.txtNom);
                EditText ETPass = findViewById(R.id.txtPrenom);
                String  userName = ETName.getText().toString();
                String  userPass = ETPass.getText().toString();
                Cursor data = db.getAllUser();
                if(!userName.equals("") && !userPass.equals("")){
                    while(data.moveToNext()){
                        String userNameInDb = data.getString(1);
                        String userPassInDb = data.getString(2);
                        int userIdInDb = data.getInt(0);
                        if( userNameInDb.equals(userName)){
                            if (userPassInDb.equals(userPass)){
                                Context.getInstance().setContextID(userIdInDb);
                                Intent contactActivity = new Intent(getApplicationContext(), RepertoireActivity.class);
                                startActivity(contactActivity);
                                finish();
                            }
                        }
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Champs incorrect",
                            Toast.LENGTH_LONG).show();
                }

            }
        });
        btnInscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactActivity = new Intent(getApplicationContext(), InscriptionActivity.class);
                startActivity(contactActivity);
                finish();
            }
        });

    }

}
