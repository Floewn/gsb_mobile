package com.floewn.gsbmobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ajout_contactActivity extends AppCompatActivity
{
    SQlite db;
    TextView nom;
    TextView prenom;
    TextView tel;
    TextView courriel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_contact);

        Button btnAdd;
        ImageButton btnReturn;
        btnReturn = findViewById(R.id.btnReturnRepertoire);
        this.db = new SQlite(this);
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nom = findViewById(R.id.txtNom);
                prenom = findViewById(R.id.txtPrenom);
                tel = findViewById(R.id.txtNumero);
                courriel = findViewById(R.id.txtCourriel);
                String userLastName = nom.getText().toString();
                String userFirstName = prenom.getText().toString();
                String userPhone = tel.getText().toString();
                String userMail = courriel.getText().toString();

                if(!userLastName.equals("") && !userFirstName.equals("")){
                    db.createContact(Context.getInstance().getContextID(), userLastName, userFirstName, userPhone, userMail);
                    nom.setText("");
                    prenom.setText("");
                    courriel.setText("");
                    tel.setText("");
                }else{
                    Toast.makeText(getApplicationContext(), "Il faut au moins un nom ainsi qu'un prenom !",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addContactActivity = new Intent(getApplicationContext(), RepertoireActivity.class);
                startActivity(addContactActivity);
                finish();
            }
        });
    }
}
