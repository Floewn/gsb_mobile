package com.floewn.gsbmobile;

public class Contact {

    private int id;
    private String nom;
    private String prenom;
    private String mail;
    private String tel;
    private int creatorID;

    public Contact(int id, String nom, String prenom, String mail, String tel, int creatorID) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.tel = tel;
        this.creatorID = creatorID;
    }
}
