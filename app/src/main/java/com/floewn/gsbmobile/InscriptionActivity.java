package com.floewn.gsbmobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class InscriptionActivity extends AppCompatActivity {

    TextView btnConnecter;
    TextView btnInscription;
    EditText nom;
    EditText pass;
    SQlite db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        this.db = new SQlite(this);
        btnConnecter = findViewById(R.id.textView23);
        btnInscription = findViewById(R.id.btnModify);
        btnConnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactActivity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(contactActivity);
                finish();
            }
        });
        btnInscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nom = findViewById(R.id.txtNom);
                pass = findViewById(R.id.txtPrenom);
                String userName = nom.getText().toString();
                String userPass = pass.getText().toString();
                if(!userName.equals("") && !userPass.equals("")){
                    db.createOrUpdateUser(nom.getText().toString(), pass.getText().toString());
                    nom.setText("");
                    pass.setText("");
                    Toast.makeText(getApplicationContext(), "Inscription terminée vous pouvez maintenant vous connecter.",
                            Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Champs incorrect",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
