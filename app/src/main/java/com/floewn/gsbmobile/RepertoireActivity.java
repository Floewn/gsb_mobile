package com.floewn.gsbmobile;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class RepertoireActivity extends AppCompatActivity
{
    SQlite db;
    ListView repertoire;
    Button btnAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repertoire);

        this.db = new SQlite(this);
        this.repertoire = findViewById(R.id.listView);

        Cursor data = db.getAllContactById(Context.getInstance().getContextID());
        String userFirstName;
        String userLastName;
        int id;
        List<String> stringContactList = new ArrayList<>();
        final List<Integer> idContactList = new ArrayList<>();
        while(data.moveToNext()){
            userFirstName = data.getString(3);
            userLastName = data.getString(2);
            id = data.getInt(0);
            stringContactList.add(userFirstName + " " + userLastName);
            idContactList.add(id);
            }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, stringContactList);
        repertoire.setAdapter(adapter);
        repertoire.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int contactId = idContactList.get(position);
                Context.getInstance().setLastContactId(contactId);
                Intent addContactActivity = new Intent(getApplicationContext(), ContactActivity.class);
                startActivity(addContactActivity);
                finish();
            }
        });

        btnAdd = findViewById(R.id.btnDelete);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addContactActivity = new Intent(getApplicationContext(), ajout_contactActivity.class);
                startActivity(addContactActivity);
                finish();
            }
        });
    }
}
