package com.floewn.gsbmobile;

 class Context {
    private static Context mInstance = null;

    private int contextID;
    private int lastContactId;


    private Context(){}

    static synchronized Context getInstance() {
        if(null == mInstance){
            mInstance = new Context();
        }
        return mInstance;
    }

    int getContextID() {
        return contextID;
    }

    void setContextID(int contextID) {
        this.contextID = contextID;
    }

    int getLastContactId() {
        return lastContactId;
    }

    void setLastContactId(int lastContactId) {
        this.lastContactId = lastContactId;
    }
}
